#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ECDSA_EOS master library of EOSCheckSig
# Copyright (C) 2019  Antoine FERRON
# Copyright (C) 2019  EOS Geneva

# Some portions based on :
# "python-ecdsa" Copyright (C) 2010 Brian Warner (MIT License)
# "Simple Python elliptic curves and ECDSA" Copyright (C) 2005 Peter Pearson (public domain)
# "Electrum" Copyright (C) 2011 thomasv@gitorious (GPL)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import os
import binascii
import base64
import struct
import hmac
from ECDSA_256k1 import *
import cPickle as pickle
from utils import *

def load_gtable(filename):
	with open(filename, 'rb') as input:
		 global gtable
		 gtable = pickle.load(input)

def mulG(real):
	if real == 0: return INFINITY
	assert real > 0
	br=[]
	dw=16
	while real > 0 :
		dm = real%dw
		real = real - dm
		br.append( dm-1 )
		real = real>>4
	while len(br)<64: br.append(-1)
	kg=INFINITY
	for n in range(64):
		if br[n]>=0:
			precomp=gtable[n][br[n]]
			kg=kg+precomp
	return kg

class Signature( object ):
	def __init__( self, pby, r, s ):
		self.r = r
		self.s = s
		self.pby = pby
	
	def encode(self):
		sigr = binascii.unhexlify(("%064x" % self.r).encode())
		sigs = binascii.unhexlify(("%064x" % self.s).encode())
		return sigr+sigs

class Public_key( object ):
	def __init__( self, point ):
		self.generator = generator_256
		self.point = point
		n =  generator_256.order()
		if not curve_256.contains_point(point.x(),point.y()):
			raise RuntimeError, "Point not on curve"
		if not n * point == INFINITY:
			raise RuntimeError, "Point order is bad."
		if point.x() < 0 or n <= point.x() or point.y() < 0 or n <= point.y():
			raise RuntimeError, "Point has x or y out of range."
	
	def verifies( self, hashe, signature ):
		if self.point == INFINITY: return False
		G = self.generator
		n = G.order()
		if not curve_256.contains_point(self.point.x(),self.point.y()): return False
		r = signature.r
		s = signature.s
		if r < 1 or r > n-1: return False
		if s < 1 or s > n-1: return False
		c = inverse_mod( s, n )
		u1 = ( hashe * c ) % n
		u2 = ( r * c ) % n
		xy =  self.point.dual_mult( u1, u2) # u1 * G + u2 * self.point
		v = xy.x() % n
		return v == r

class Private_key( object ):
	def __init__( self, secret_multiplier ):
		#self.public_key = public_key
		self.secret_multiplier = secret_multiplier
	
	def der( self ):
		hex_der_key = '06052b8104000a30740201010420' + \
					  '%064x' % self.secret_multiplier + \
					  'a00706052b8104000aa14403420004' + \
					  '%064x' % self.public_key.point.x() + \
					  '%064x' % self.public_key.point.y()
		return hex_der_key.decode('hex')
	
	def sign( self, hash, k ):
		G = generator_256 #self.public_key.generator
		n = G.order()
		p1 = mulG(k)
		r = p1.x()
		if r == 0: raise RuntimeError, "amazingly unlucky random number r"
		s = ( inverse_mod( k, n ) * ( hash + ( self.secret_multiplier * r ) % n ) ) % n
		if s == 0: raise RuntimeError, "amazingly unlucky random number s"
		if s > (n>>1): #Canonical Signature enforced (lower S)
			s = n - s
			pby = (p1.y()+1)&1
		else:
			pby = (p1.y())&1
		return Signature( pby, r, s )

def randoml(pointgen):
	cand = 0
	while cand<1 or cand>=pointgen.order():
		cand=int(os.urandom(32).encode('hex'), 16)
	return cand

def hash_msg(message):
	return h2i(hashlib.sha256(message).hexdigest())

def sig_msg_verify(msg, sig, pubkeyb58):
	try:
		# Check publicKey format and checksum
		pubhex = pubkeystr_to_hex(pubkeyb58)
		px, py = publkey(pubhex)
		# Check Q is valid : Q!=0, on curve, n.Q = 0
		Q = Public_key(Point( px, py ))
		# Check signature format and checksum
		flag, r, s = decode_sig(sig)
		hash = hash_msg(msg)
	except:
		return False
	return sig_verify(flag, r, s, hash, Q)

def sig_verify(flag, r, s, hash, publickey):
		try:
			y = recoverY(r, flag+1)
			R = Point(r, y, generator_256.order() )
		except:
			return False
		return check_sig(hash, R, s, publickey)

def check_sig(hash, R, s, publickey):
		r = R.x()
		y = R.y()
		order = generator_256.order()
		if r <= 0 or r > order-1:
			return False
		if s <= 0 or s > order-1:
			return False
		# check R is on curve
		if not curve_256.contains_point(r,y):
			return False
		# check that n.R is at infinity
		if not order*R==INFINITY:
			return False
		inv_s = inverse_mod(s, order)
		# Rcomp = h/s.G + r/s.Q
		Rcomp = publickey.point.dual_mult( (hash*inv_s)%order, (r*inv_s)%order )
		return Rcomp == R

