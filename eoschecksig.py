#!/usr/bin/env python
# -*- coding: utf-8 -*-

# eoschecksig main command line of EOSCheckSig
# Copyright (C) 2019  Antoine FERRON
# Copyright (C) 2019  EOS Geneva

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import sys
from ECDSA_EOS import *

# command : python eoschecksig.py message EOSpubkey SIG_K1_eossignature

if __name__ == "__main__":
	if len(sys.argv) == 4:
		try:
			msg = sys.argv[1]
			pubb58 = sys.argv[2]
			sig = sys.argv[3]
			if sig_msg_verify(msg, sig, pubb58):
				print "GOOD"
			else:
				print "BAD"
		except:
			print "BAD"
	else:
		print "BAD"
