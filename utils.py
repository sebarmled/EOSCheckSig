#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Utils encoding tools library of EOSCheckSig
# Copyright (C) 2019  Antoine FERRON
# Copyright (C) 2019  EOS Geneva

# Some portions based on :
# "python-ecdsa" Copyright (C) 2010 Brian Warner (MIT License)
# "Simple Python elliptic curves and ECDSA" Copyright (C) 2005 Peter Pearson (public domain)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import hashlib
from ECDSA_256k1 import recoverY

b58chars = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'

def hexa(cha):
	hexas = hex(cha)[2:-1]
	while len(hexas)<64:
		hexas = "0"+hexas
	return hexas

def base58_to_hex(base58):
	hex_data = ''
	int_data = 0
	p58 = 1
	for i in xrange(-1, -len(base58)-1, -1):
		int_data += (b58chars.index(base58[i]))*p58
		p58 *= 58
	hex_data = hex(int_data)[2:-1]
	for i in xrange(len(base58)):
		if base58[i] == '1':
			hex_data = '00' + hex_data
		else:
			break
	return hex_data

def h2i(hexstr):
	return int(hexstr,16)

def compute_checksum(pubhkey):
	h160 = hashlib.new('ripemd160')
	h160.update(pubhkey.decode('hex'))
	return h160.hexdigest()[0:8]

def pubkeystr_to_hex(publ):
	assert publ[0:3] == "EOS"
	pubh = base58_to_hex(publ[3:])
	pubhkey = "0"+pubh[0:-8]
	assert pubh[-8:] == compute_checksum(pubhkey)
	return pubhkey

def publkey(pubhex):  # hex to int x y
	datapar = h2i(pubhex[0:2])-2
	x = h2i(pubhex[2:66])
	y = recoverY(x, datapar)
	return x,y

def sig_to_hex(sig):
	assert sig[0:7] == "SIG_K1_"
	sigh = base58_to_hex(sig[7:])
	sighkey = sigh[0:-8]
	assert sigh[-8:] == compute_checksum(sighkey+"4B31")
	return sighkey

def read_hexsig(hexsig):
	return ( h2i(hexsig[0:2]), h2i(hexsig[2:66]), h2i(hexsig[66:]) )

def decode_sig(sig):
	sighex = sig_to_hex(sig)
	return read_hexsig(sighex)

