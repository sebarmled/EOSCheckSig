
# EOSCheckSig

A Python script that checks an EOS "K1" signature with a provided publickey.

## Installation

Nothing special required. It runs on standard Python v2.7.


## Using EOSSigCheck


`./eossigcheck.py message EOSpubkey SIG_K1_eossignature`       or

`python eossigcheck.py MESSAGE EOSpubkey SIG_K1_eossignature`

It returns "BAD" or "GOOD" in case all checks are OK.

## Performance

It takes around 50ms on a standard CPU core clocked at 2.5GHz to perform a full EOS signature decoding and verification.


## License

Copyright (C) 2019  Antoine FERRON

Copyright (C) 2019  EOS Geneva


Some portions based on 
"python-ecdsa" Copyright (C) 2010 Brian Warner (MIT Licence) 
"Simple Python elliptic curves and ECDSA" Copyright (C) 2005 Peter Pearson (public domain) 
"Electrum" Copyright (C) 2011 thomasv@gitorious (GPL) 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

